from django.conf.urls import url
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from .views import MurrenViewSet, GoogleLogin


router = DefaultRouter()
router.register('', MurrenViewSet)

urlpatterns = [
    url('token_create/', obtain_jwt_token, name='obtain_jwt_token'),
    path('oauth/google/', GoogleLogin.as_view(), name='socialaccount_signup')

]

urlpatterns += router.urls
