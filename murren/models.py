from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


def murren_upload_to_path_avatar(instance, tmp_image_name):
    filename = str(instance.id) + ".jpeg"
    return f'profile/avatar/{filename}'


class Murren(AbstractUser):
    email = models.EmailField(unique=True)
    murren_avatar = models.ImageField(default='default_murren_avatar.png', upload_to=murren_upload_to_path_avatar,
                                      verbose_name='Аватар Муррена')
    is_banned = models.BooleanField(_('banned'), default=False,
                                    help_text=_('Designates whether this user should be treated as banned.'))

    def __str__(self):
        return self.username
