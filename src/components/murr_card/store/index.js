import actions from "./actions.js";
import mutations from "./mutations.js";

export default {
  state: {
    murrList: [],
    murrListCount: 0,
    murr: null,
  },
  getters: {
    murrList: (state) => state.murrList,
    murr: ({ murr }) => {
      if (!murr) {
        return murr;
      }

      const content = JSON.parse(murr.content).blocks;

      return {
        ...murr,
        content,
      };
    },
  },
  actions,
  mutations,
};
