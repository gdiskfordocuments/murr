import { MURREN_SET_PROFILE } from "./type.js";

export default {
  [MURREN_SET_PROFILE]: (state, payload) => (state.profileData = payload),
};
