import axios from "axios";

export const actions = {
  async changeShownSignUpForm_actions(context) {
    context.commit("changeShownRegisterForm_mutations");
  },
  async changeShowLoginForm_actions(context) {
    context.commit("changeShowLoginForm_mutations");
  },
  async changeShowResetPasswordForm_actions(context) {
    context.commit("changeShowResetPasswordForm_mutations");
  },
  async changeShowCreateMurr_actions(context) {
    context.commit("changeShowCreateMurr_mutations");
  },
  async changeSaveMurrContent_action(context, data) {
    context.commit("changeSaveMurrContent_mutations", data);
  },
  async fetchMurrCards(context, page = null) {
    try {
      let url = "/api/murr_card/";
      if (page) {
        url += `?page=${page}`;
      }
      const { data } = await axios.get(url);
      if (data.results) {
        await context.commit("setMurrCards", data.results);
        await context.commit("setMurrCardsCount", data.count);
      }
      if (data.next) {
        await context.commit("setNextMurrCardsPage", data.next.split("=")[1]);
      } else {
        await context.commit("setNextMurrCardsPage", false);
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  },
};
