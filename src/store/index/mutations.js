export const mutations = {
  changeShownRegisterForm_mutations(state) {
    state.showRegisterForm = !state.showRegisterForm;
  },
  changeShowLoginForm_mutations(state) {
    state.showLoginForm = !state.showLoginForm;
  },
  changeShowResetPasswordForm_mutations(state) {
    state.showResetPasswordForm = !state.showResetPasswordForm;
  },
  changeShowCreateMurr_mutations(state) {
    state.showCreateMurr = !state.showCreateMurr;
  },
  changeSaveMurrContent_mutations(state, data) {
    state.murrContent = data.murrContent;
    state.murrHeader = data.murrHeader;
  },
  setMurrCards(state, murrCards) {
    state.murrCards = [...state.murrCards, ...murrCards];
  },
  setMurrCardsCount(state, count) {
    state.murrCardsCount = count
  },
  clearMurrCards(state) {
    state.murrCards = [];
  },
  setNextMurrCardsPage(state, nextPage) {
    state.nextMurrCardsPage = nextPage;
  },
};
