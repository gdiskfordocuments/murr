from django.contrib import admin
from .models import Comment


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    search_fields = ['author__username']
    ordering = ('-rating',)
    list_display = ('created', 'rating', 'author')
